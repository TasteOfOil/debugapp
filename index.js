function sumArr(array){
    var sum = 0;
    for(let i = 0;i<array.length;i++){
        sum = sum+array[i];
    }
    return sum;
}


var arr = [1,2,3];
const result = sumArr(arr);
if(result){
    console.log(`True. Result = ${result}`);
}
else {
    console.log(`False. Result = ${result}`);
}